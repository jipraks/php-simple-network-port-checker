<?php

    error_reporting(0);

    echo PHP_EOL . PHP_EOL;

    $val = getopt("h:p:c:o:");

    if(isset($val['h']) && isset($val['p']) && isset($val['c']) && isset($val['o'])) {

        $host       = $val['h'];
        $ip         = gethostbyname($host);
        $port       = $val['p'];
        $loop       = $val['c'];
        $ttl        = $val['o'];
        $totalMs    = 0;
        $ok         = 0;
        $nok        = 0;

        echo "Testing connection to port " . $port . " on " . $host . " (" . $ip . ")" . PHP_EOL;

        for($x = 0; $x < $loop; $x++) {

            $starttime = (microtime(true) * 1000);

            $fp = fsockopen($ip , $port, $errno, $errstr, $ttl);

            if (!$fp) {

                $nok++;
                $endtimes = number_format((microtime(true) * 1000) - $starttime, 2);
                echo "NOT CONNECTED TO " . $host . " (" . $endtimes . " ms) | Success : " .$ok . " (".number_format(($ok / ($ok + $nok)) * 100, 2)."%) | Failed : " . $nok . " (".number_format(($nok / ($ok + $nok)) * 100, 2)."%)" . PHP_EOL;

                

            } else {
                
                $ok++;
                $endtime = number_format((microtime(true) * 1000) - $starttime, 2);
                echo "CONNECTED TO " . $host . " (" . $endtime . " ms) | Success : " .$ok . " (".number_format(($ok / ($ok + $nok)) * 100, 2)."%) | Failed : " . $nok . " (".number_format(($nok / ($ok + $nok)) * 100, 2)."%)" . PHP_EOL;
                
            }

            $totalMs = $totalMs + $endtime;
            sleep(1);

        }

        $averageMs = number_format($totalMs / $loop, 2);

        echo "Finish testing connection to port " . $port . " on " . $host . " (" . $ip . ")" . PHP_EOL;
        echo PHP_EOL;
        echo "Total elapsed time : " . $totalMs . " ms || ";
        echo "Average time : " . $averageMs . " ms || ";

        echo "Success transmission : " . $ok ." || ";
        echo "Failed transmission : " . $nok ." || ";
        echo "Success / Failed rate : (" . number_format(($ok / ($ok + $nok)) * 100, 2) . "% / " . number_format(($nok / ($ok + $nok)) * 100, 2) . "%)" . PHP_EOL . PHP_EOL;   


    } else {


        echo "[ERROR] Parameters incompleted." . PHP_EOL;

    }

?>