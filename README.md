# PHP Simple Network Port Checker

Simple network port checker using fsockopen() function on PHP. This simple tools is useful for testing connection on network using specific port. 

## Usage

```
php checker.php -h <hostname> -p <port> -c <loop count> -o <timeout>
```
